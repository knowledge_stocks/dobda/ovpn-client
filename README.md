# 사용법

## client.ovpn 파일 준비

자신의 클라이언트 접속 정보 파일을 src 폴더에 client.ovpn이라는 이름으로 추가한다.\
라우팅 설정이 귀찮으므로 tun 대신 tap을 사용하자.

## 도커 이미지 빌드

```bash
# Dockerfile이 있는 위치에서 실행
$ docker build -t ovpn-client .
```

## 컨테이너 실행하기

### 1. Docker compose로 실행하기

```bash
# docker-compose.yml에 계정, 비밀번호 설정
$ nano docker-compose.yml
# 기존 컨테이너 제거
$ docker-compose down
# 컨테이너 시작
$ docker-compose up -d
```

### 2. Docker run으로 실행하기

```bash
$ docker run --rm -d --restart unless-stopped --net=host --cap-add=NET_ADMIN --name ovpn-client ovpn-client {username} {password}
```
